<?php

/**
 * @file
 * Contains page callbacks class.
 */
class MyPages {

  /**
   * Override default page callback.
   */
  public static function node() {
    drupal_set_title('');
    $bucket = 'https://bitbucket.org/hfccwebdev/sites_its';
    return t('Custom code for this website is published at !url. Thanks for visiting.', ['!url' => l($bucket, $bucket)]);
  }

  /**
   * Redirect front page traffic based on user.
   */
  public static function front() {
    global $user;
    if ($user->uid !== 0) {
      $target = variable_get('mysite_redirect_registered', 'node');
      drupal_goto($target);
    }
    // Render a static homepage from template for non-authenticated users.
    return [
      '#theme' => 'its_homepage',
    ];
  }

  /**
   * Page callback for taxonomy teams vocabulary.
   *
   * Rewrite only the content portion of this page using views.
   * The rest of the code here is a direct copy of the core function
   * taxonomy_term_page() function.
   *
   * @see taxonomy_term_page()
   * @see https://chacadwa.com/blog/2012/12/08/override-drupal-7-taxonomy-display-vocabulary
   *
   * @todo Get rid of this and override taxonomy_term_uri() instead.
   */
  public static function taxonomyTermPage($term) {
    drupal_set_title($term->name);
    $current = (object) ['tid' => $term->tid];
    $breadcrumb = [];
    while ($parents = taxonomy_get_parents($current->tid)) {
      $current = array_shift($parents);
      $breadcrumb[] = l($current->name, 'taxonomy/term/' . $current->tid);
    }
    $breadcrumb[] = l(t('Home'), NULL);
    $breadcrumb = array_reverse($breadcrumb);
    drupal_set_breadcrumb($breadcrumb);
    $uri = entity_uri('taxonomy_term', $term);

    // Set the term path as the canonical URL to prevent duplicate content.
    drupal_add_html_head_link(['rel' => 'canonical', 'href' => url($uri['path'], $uri['options'])], TRUE);
    // Set the non-aliased path as a default shortlink.
    drupal_add_html_head_link(['rel' => 'shortlink', 'href' => url($uri['path'], array_merge($uri['options'], ['alias' => TRUE]))], TRUE);

    $build = taxonomy_term_show($term);

    // This is the only deviation from the core function.

    switch($term->vocabulary_machine_name) {
      case 'categories':
        $build['members'] = array('#markup' => views_embed_view('categories', 'block', $term->tid));
        break;
      case 'document_lists':
        $build['members'] = array('#markup' => views_embed_view('document_lists', 'block', $term->tid));
        break;
      default:
        // Show the default page instead.
        return taxonomy_term_page($term);
    }
    return $build;
  }
}
