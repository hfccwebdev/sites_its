<?php

/**
 * @file
 * Callbacks for custom blocks.
 */
class MyBlocks {

  /**
   * Callback for Big Brother block.
   *
   * Generate Big Brother monitor block contents.
   */
  public static function bbStatus() {
    if ($bburl = variable_get('mysite_bbrequest', FALSE)){
      $bbrequest = drupal_http_request($bburl);
      if (isset($bbrequest->data)) {
        $bbstatus = array('#markup' => $bbrequest->data);
      }
      else {
        $bbstatus = array('#markup' => t('Big Brother error: @error', array('@error' => $bbrequest->error)));
      }
      return $bbstatus;
    }
    else {
      drupal_set_message(t('Please set mysite_bbrequest variable.'), 'warning');
    }
  }

  /**
   * Generate a block that shows information about user's connection.
   */
  public static function connInfo() {
    global $base_url;
    $rows = array();
    $rows[] = t('Base URL: @url', array('@url' => $base_url));
    $rows[] = t('Forwarded for: @forward', array('@forward' => !empty($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : 'NONE'));
    $rows[] = t('Client IP: @client', array('@client' => !empty($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : 'NONE'));
    return array(
      '#theme' => 'item_list',
      '#items' => $rows,
    );
  }

  /**
   * Generate custom topic list.
   *
   * This function replaces the default taxonomy listing by only showing
   * topics that contain nodes visible to the current viewer. This filters
   * out nodes protected by the Privacy module.
   */
  public static function topics() {

    $query = db_select('node', 'n')
      ->condition('n.status', 1)
      ->condition('n.type', 'page')
      ;
    $query->leftjoin('field_data_field_categories', 'c', "n.nid = c.entity_id AND n.vid = c.revision_id AND c.entity_type = 'node'");
    $query->isNotNull('c.field_categories_tid');
    $query->leftjoin('taxonomy_term_data', 't', "c.field_categories_tid = t.tid");
    $query->fields('t', array('tid', 'name'));

    if (module_exists('private') && !user_access('access private content')) {
      $query->leftjoin('private', 'p', "n.nid = p.nid");
      $query->condition('p.private', 0);
    }

    $query->distinct();
    $query->orderBy('t.name');

    $result = $query->execute()->fetchAll();

    $rows = array();
    foreach ($result as $term) {
      $options = array('attributes' => array('class' => array('taxonomy-term-' . $term->tid)));
      $rows[] = array(
        'data' => l($term->name, 'taxonomy/term/'. $term->tid, $options),
        'class' => array('leaf'),
      );
    }
    if (!empty($rows)) {
      return array(
        '#theme' => 'item_list',
        '#items' => $rows,
        '#attributes' => array('class' => 'menu'),
      );
    }
  }
}
