<?php

/**
 * @file
 * Contains custom code specific to this site.
 */

/**
 * Implements hook_views_api().
 */
function mysite_views_api() {
  return [
    'api' => 3,
    'path' => drupal_get_path('module', 'mysite') . '/views',
  ];
}

/**
 * Implements hook_menu().
 */
function mysite_menu() {
  return [
    'api/1.0/file/index' => [
      'title' => 'File API Index',
      'page callback' => 'MysiteApiController::fileIndex',
      // Access check will be performed by API endpoint.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ],
    'api/1.0/file/%file' => [
      'title' => 'File',
      'page callback' => 'MysiteApiController::file',
      'page arguments' => [3],
      // Access check will be performed by API endpoint.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ],
    'api/1.0/%/index' => [
      'title' => 'Content API Index',
      'page callback' => 'MysiteApiController::index',
      'page arguments' => [2],
      // Access check will be performed by API endpoint.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ],
    'api/1.0/node/%node' => [
      'title callback' => 'node_page_title',
      'title arguments' => [3],
      'page callback' => 'MysiteApiController::content',
      'page arguments' => [3],
      // Access check will be performed by API endpoint.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ],
    'api/1.0/tags' => [
      'title' => 'Tags API Index',
      'page callback' => 'MysiteApiController::tags',
      // Access check will be performed by API endpoint.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ],
    'api/1.0/users' => [
      'title' => 'User API Index',
      'page callback' => 'MysiteApiController::users',
      // Access check will be performed by API endpoint.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ],
    'front' => [
      'title' => 'Home',
      'page callback' => 'MyPages::front',
      // Access check will be performed by callback.
      'access callback' => TRUE,
      'type' => MENU_CALLBACK,
    ],
  ];
}

/**
 * Implements hook_menu_alter().
 */
function mysite_menu_alter(&$items) {

  // Override the default homepage content.
  $items['node']['page callback'] = 'MyPages::node';

  // Override taxonomy term page content.
  $items['taxonomy/term/%taxonomy_term']['page callback'] = 'MyPages::taxonomyTermPage';
}

/**
 * Implements hook_block_info().
 *
 * This hook declares what blocks are provided by the module.
 */
function mysite_block_info() {
  $blocks = [];
  $blocks['mysite_bbstatus'] = ['info' => t('Big Brother status'), 'cache' => DRUPAL_NO_CACHE];
  $blocks['mysite_connection_info'] = ['info' => t('Connection info'), 'cache' => DRUPAL_NO_CACHE];
  $blocks['mysite_topics'] = ['info' => t('Topics list'), 'cache' => DRUPAL_CACHE_PER_ROLE];
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function mysite_block_view($delta = '') {
  $block = [];
  switch ($delta) {
    case 'mysite_bbstatus':
      $block['subject'] = t('Big Brother status');
      $block['content'] = MyBlocks::bbStatus();
      break;

    case 'mysite_connection_info':
      $block['subject'] = t('Connection Info');
      $block['content'] = MyBlocks::connInfo();
      break;

    case 'mysite_topics':
      $block['subject'] = t('Topics');
      $block['content'] = MyBlocks::topics();
      break;
  }
  return $block;
}

/**
 * Implements hook_form_alter().
 */
function mysite_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'system_site_information_settings':
      $form['front_page']['mysite_redirect_anon'] = [
        '#type' => 'textfield',
        '#title' => t('Anonymous redirect target'),
        '#default_value' => variable_get('mysite_redirect_anon', NULL),
        '#size' => 40,
        '#description' => t('Specify a relative URL or external path to redirect anonymous users.'),
        '#required' => TRUE,
      ];
      $form['front_page']['mysite_redirect_registered'] = [
        '#type' => 'textfield',
        '#title' => t('Registered users redirect target'),
        '#default_value' => variable_get('mysite_redirect_registered', NULL),
        '#size' => 40,
        '#description' => t('Specify a relative URL or external path to redirect registered users.'),
        '#required' => TRUE,
      ];
      break;
  }
}

/**
 * Implements hook_hfccbane_news_list_types_alter().
 */
function mysite_hfccbane_news_list_types_alter(&$list_types) {
  $list_types['mysite_hfccnews'] = t('Teaser list - no photos');
}

/**
 * Implements hook_theme().
 */
function mysite_theme() {
  return [
    'mysite_hfccnews' => [
      'render element' => 'elements',
      'template' => 'mysite_hfccnews',
      'file' => 'mysite.theme.inc',
    ],
    'its_homepage' => [
      'render element' => 'nothing',
      'template' => 'its_homepage',
    ],
  ];
}

/**
 * Implements hook_menu_breadcrumb_alter().
 */
function mysite_menu_breadcrumb_alter(&$active_trail, $item) {
  if (!drupal_is_front_page()) {
    // Quick fix. If breadcrumb will only include "Home" then nuke it.
    $count = count($active_trail);
    if ($count <= 2) {
      $active_trail = array();
    }
  }
}

/**
 * Access callback for bot overview.
 */
function mysite_access_bot_overview() {

  $perms = ['access bot factoids', 'access bot karma scores', 'view bot logs'];

  foreach (variable_get('bot_log_channels', array()) as $channel) {
    $perms[] = "view bot logs: $channel";
  }

  $result = FALSE;
  foreach ($perms as $perm) {
    $result = $result || user_access($perm);
  }
  return $result;
}
