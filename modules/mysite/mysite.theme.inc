<?php

/**
 * @file
 * Theme functions for mysite module.
 */

/**
 * Processes variables for mysite_hfccnews.tpl.php
 */
function template_preprocess_mysite_hfccnews(&$variables) {
  $entity = $variables['elements'];

  $variables['title'] = $entity['title'];
  $variables['page'] = !empty($entity['#page']) && $entity['#page'];
  $variables['news_url'] = url('news/items/' . $entity['legacy_id']);
  $variables['alternative_link'] = !empty($entity['alternative_link']) ? $entity['alternative_link'] : NULL;

  $content = [];

  if (!empty($entity['cut_line']['safe_value'])) {
    $content['body'] = ['#prefix' => '<p>', '#markup' => $entity['cut_line']['safe_value'], '#suffix' => '</p>'];
  }
  elseif (!empty($entity['body']['summary'])) {
    $content['body'] = ['#markup' => check_markup($entity['body']['summary'], $entity['body']['format'])];
  }
  else {
    $body = check_markup($entity['body']['value'], $entity['body']['format']);
    $teaser_length = variable_get('hfccbane_page_teaser_length', variable_get('teaser_length', 600));
    $content['body'] = ['#markup' => text_summary($body, $entity['body']['format'], $teaser_length)];
  }
  $content['links'] = ['#markup' => l(t('read more'), 'news/items/' . $entity['legacy_id'])];

  $variables['content'] = $content;
  $variables['news'] = $entity;
}
