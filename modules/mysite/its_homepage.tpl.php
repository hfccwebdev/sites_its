<?php

/**
 * @file
 * Content for the ITS homepage.
 */
?>
<h1>IT Services internal information</h1>

<p>This site contains internal information for IT Services only.</p>

<p>To contact IT Services, please <a href="https://www.hfcc.edu/helpdesk">visit the Help Desk</a>.</p>

