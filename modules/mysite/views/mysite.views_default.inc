<?php

/**
 * @file
 * Loads default views.
 *
 * This file is automatically loaded by Drupal.
 *
 * @see hook_views_default_views()
 */

/**
 * Implementation of hook_views_default_views().
 */
function mysite_views_default_views() {
  $views = array();
  $files = file_scan_directory(drupal_get_path('module', 'mysite') . '/views/views', '/.inc$/');
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }
  return $views;
}
