<?php

/**
 * @file
 * Defines the Recently Active Users view.
 */

$view = new view();
$view->name = 'recently_active_users';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'users';
$view->human_name = 'Recently active users';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Recently active users';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  7 => '7',
  4 => '4',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'field_user_hank_id' => 'field_user_hank_id',
  'name' => 'name',
  'field_user_full_name' => 'field_user_full_name',
  'mail' => 'mail',
  'field_user_network_role' => 'field_user_network_role',
  'field_user_constituency' => 'field_user_constituency',
  'created' => 'created',
  'access' => 'access',
  'edit_node' => 'edit_node',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_user_hank_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_user_full_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'mail' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_user_network_role' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_user_constituency' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'access' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['sticky'] = TRUE;
/* Field: User: HANK ID */
$handler->display->display_options['fields']['field_user_hank_id']['id'] = 'field_user_hank_id';
$handler->display->display_options['fields']['field_user_hank_id']['table'] = 'field_data_field_user_hank_id';
$handler->display->display_options['fields']['field_user_hank_id']['field'] = 'field_user_hank_id';
$handler->display->display_options['fields']['field_user_hank_id']['alter']['alt'] = 'GetPersonInfo for this user';
$handler->display->display_options['fields']['field_user_hank_id']['alter']['target'] = '_blank';
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
/* Field: User: Full Name */
$handler->display->display_options['fields']['field_user_full_name']['id'] = 'field_user_full_name';
$handler->display->display_options['fields']['field_user_full_name']['table'] = 'field_data_field_user_full_name';
$handler->display->display_options['fields']['field_user_full_name']['field'] = 'field_user_full_name';
/* Field: User: E-mail */
$handler->display->display_options['fields']['mail']['id'] = 'mail';
$handler->display->display_options['fields']['mail']['table'] = 'users';
$handler->display->display_options['fields']['mail']['field'] = 'mail';
/* Field: User: Network Role */
$handler->display->display_options['fields']['field_user_network_role']['id'] = 'field_user_network_role';
$handler->display->display_options['fields']['field_user_network_role']['table'] = 'field_data_field_user_network_role';
$handler->display->display_options['fields']['field_user_network_role']['field'] = 'field_user_network_role';
/* Field: User: Constituency */
$handler->display->display_options['fields']['field_user_constituency']['id'] = 'field_user_constituency';
$handler->display->display_options['fields']['field_user_constituency']['table'] = 'field_data_field_user_constituency';
$handler->display->display_options['fields']['field_user_constituency']['field'] = 'field_user_constituency';
/* Field: User: Created date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'users';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['date_format'] = 'custom';
$handler->display->display_options['fields']['created']['custom_date_format'] = 'n/j/Y';
/* Field: User: Last access */
$handler->display->display_options['fields']['access']['id'] = 'access';
$handler->display->display_options['fields']['access']['table'] = 'users';
$handler->display->display_options['fields']['access']['field'] = 'access';
$handler->display->display_options['fields']['access']['date_format'] = 'custom';
$handler->display->display_options['fields']['access']['custom_date_format'] = 'n/j/Y';
/* Field: User: Edit link */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'users';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
/* Sort criterion: User: Last access */
$handler->display->display_options['sorts']['access']['id'] = 'access';
$handler->display->display_options['sorts']['access']['table'] = 'users';
$handler->display->display_options['sorts']['access']['field'] = 'access';
$handler->display->display_options['sorts']['access']['order'] = 'DESC';
/* Filter criterion: User: Active */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'users';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/people/recent';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Recent Users';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
