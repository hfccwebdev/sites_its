<?php

/**
 * @file
 * Contains classes to define the ServerStatus object.
 */

class ServerStatus {

  /**
   * Stores the server info.
   * @var StdClass $server
   */
  protected $server;

  /**
   * Create an instance of this class.
   *
   * @param string $name
   *   The name of the server info to fetch.
   */
  public static function create($name) {
    return new static($name);
  }

  /**
   * Initialize the class.
   *
   * @param string $name
   *   The name of the server info to fetch.
   */
  public function __construct($name) {
    $this->server = $this->fetch_server_data($name);
  }

  /**
   * Fetch data from the remote server.
   *
   * @todo parameterize the source database connection info.
   */
  private function fetch_server_data($name) {
    $connection_name = variable_get('server_status_database_connection', 'netadmin');
    try {
      $query = Database::getConnection('default', $connection_name)->select('status_report', 's');
      $query->fields('s');
      $query->condition('SERVER', $name);
      $result = $query->execute()->fetchAll();
      return !empty($result) ? reset($result) : NULL;
    }
    catch (Exception $e) {
      drupal_set_message(t('Error reading server status: %e', array('%e' => $e->getMessage())), 'error');
    }
  }

  /**
   * Returns raw data.
   */
  public function sushi() {
    return $this->server;
  }

  /**
   * Returns renderable array.
   */
  public function content() {
    if (!empty($this->server)) {
      $output = array();

      $output['updated'] = array(
        '#theme' => 'hfcc_global_pseudo_field',
        '#field_name' => 'status_report_updated',
        '#label' => t('Updated'),
        '#label_display' => 'inline',
        '#markup' => format_date(strtotime($this->server->UPDATED), 'long'),
      );

      $output['sysstat'] = array(
        '#theme' => 'hfcc_global_pseudo_field',
        '#field_name' => 'status_report_sysstat',
        '#label' => t('System status'),
        '#items' => array(array(
          '#prefix' => '<pre>',
          '#markup' => check_plain($this->server->SYSSTAT),
          '#suffix' => '</pre>',
        )),
      );

      if (!empty($this->server->COPS)) {
        $output['cops'] = array(
          '#theme' => 'hfcc_global_pseudo_field',
          '#field_name' => 'status_report_cops',
          '#label' => t('Security Report'),
          '#items' => array(array(
            '#prefix' => '<pre>',
            '#markup' => check_plain($this->server->COPS),
            '#suffix' => '</pre>',
          )),
        );
      }

      if (!empty($this->server->DF)) {
        $output['df'] = array(
          '#theme' => 'hfcc_global_pseudo_field',
          '#field_name' => 'status_report_df',
          '#label' => t('Disk Space'),
          '#items' => array(array(
            '#prefix' => '<pre>',
            '#markup' => check_plain($this->server->DF),
            '#suffix' => '</pre>',
          )),
        );
      }

      if (!empty($this->server->YUM)) {
        $output['yum'] = array(
          '#theme' => 'hfcc_global_pseudo_field',
          '#field_name' => 'status_report_yum',
          '#label' => t('Updates needed'),
          '#items' => array(array(
            '#prefix' => '<pre>',
            '#markup' => check_plain($this->server->YUM),
            '#suffix' => '</pre>',
          )),
        );
      }

      if (!empty($this->server->LASTYUM)) {
        $output['lastyum'] = array(
          '#theme' => 'hfcc_global_pseudo_field',
          '#field_name' => 'status_report_lastyum',
          '#label' => t('Last yum'),
          '#label_display' => 'inline',
          '#markup' => format_date(strtotime($this->server->LASTYUM), 'custom', 'm/d/Y'),
        );
      }

      if (!empty($this->server->MAILQ)) {
        $output['mailq'] = array(
          '#theme' => 'hfcc_global_pseudo_field',
          '#field_name' => 'status_report_mailq',
          '#label' => t('Mail queue'),
          '#items' => array(array(
            '#prefix' => '<pre>',
            '#markup' => check_plain($this->server->MAILQ),
            '#suffix' => '</pre>',
          )),
        );
      }

      if (!empty($this->server->CUSTOM)) {
        $output['custom'] = array(
          '#theme' => 'hfcc_global_pseudo_field',
          '#field_name' => 'status_report_custom',
          '#label' => t('Additional information'),
          '#items' => array(array(
            '#prefix' => '<pre>',
            '#markup' => check_plain($this->server->CUSTOM),
            '#suffix' => '</pre>',
          )),
        );
      }

      return $output;
    }
  }

}
