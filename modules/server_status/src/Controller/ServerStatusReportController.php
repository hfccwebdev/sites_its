<?php

/**
 * Defines the Server Status Report Controller.
 */
class ServerStatusReportController {

  /**
   * Stores the current report name.
   */
  private $reportName;

  /**
   * Displays the report.
   *
   * @param string $report_name
   *   The report name
   */
  public static function view() {
    $parameters = drupal_get_query_parameters();
    $report_name = $parameters['report'] ?? 'HFCC';
    $report = new static($report_name);
    return $report->build();
  }

  /**
   * Class constructor.
   *
   * @param string $report_name
   *   The report name
   */
  public function __construct($report_name) {
    $this->reportName = $report_name;
  }

  /**
   * Builds the report output.
   */
  public function build() {

    $output = [];

    $output[] = [
      '#prefix' => '<div class="server-update-report">',
      'heading' => ['#markup' => t('<h2>Last Update Times</h2>')],
      'content' => $this->getRecentUpdates(),
      '#suffix' => '</div>',
    ];

    $output[] = [
      '#prefix' => '<div class="server-health-report">',
      'heading' => ['#markup' => t('<h2>Health Report</h2>')],
      'content' => $this->getHealthReport(),
      '#suffix' => '</div>',
    ];

    return $output;
  }

  /**
   * Gets content for Recent Updates list.
   */
  private function getRecentUpdates() {
    $query = "SELECT SERVER,UPDATED FROM status_report WHERE REPORT=:report_name ORDER BY UPDATED, SERVER";
    $args = ['report_name' => $this->reportName];
    $result = $this->dbQuery($query, $args);
    return array_map('self::formatRecentUpdateRow', $result);
  }

  /**
   * Format rows for Recent Updates list.
   */
  private function formatRecentUpdateRow($item) {

    $classes_array = ['update-info'];
    if (strtotime($item->UPDATED) + 86400 < REQUEST_TIME) {
      $classes_array[] = 'stale';
    }
    $classes = implode(' ', $classes_array);

    return [
      '#prefix' => "<div class=\"$classes\">",
      '#markup' => t(
        '<span class="server-name"><a href="#health-@anchor">@name</a>:</span> <span class="update-time">@time</span>',
        ['@anchor' => mb_strtolower($item->SERVER), '@name' => $item->SERVER, '@time' => $item->UPDATED]
      ),
      '#suffix' => '</div>',
    ];
  }

  /**
   * Get content for Health Report.
   */
  private function getHealthReport() {
    $query = "SELECT SERVER, UPDATED, SYSSTAT, YUM, LASTYUM FROM status_report WHERE REPORT=:report_name ORDER BY SERVER";
    $args = ['report_name' => $this->reportName];
    $result = $this->dbQuery($query, $args);
    return array_map('self::formatHealthReport', $result);
  }

  /**
   * Format Health Report output.
   */
  private function formatHealthReport($server) {

    $output = [
      '#prefix' => '<div class="server-health-details">',
      '#suffix' => '</div>',
    ];

    $servername = check_plain($server->SERVER);
    if ($path = $this->getNodeByName($servername)) {
      $servername = l($servername, $path);
    }

    $output['server'] = [
      '#markup' => t('<h3 id="health-@anchor">!name</h3>', ['@anchor' => mb_strtolower($server->SERVER), '!name' => $servername]),
    ];

    $output['sysstat'] = [
      '#theme' => 'hfcc_global_pseudo_field',
      '#field_name' => 'status_report_sysstat',
      '#label' => t('System status'),
      '#markup' => '<pre>' . check_plain($server->SYSSTAT) . '</pre>',
    ];

    if (!empty($server->YUM)) {
      $output['yum'] = [
        '#theme' => 'hfcc_global_pseudo_field',
        '#field_name' => 'status_report_yum',
        '#label' => t('Updates needed'),
        '#markup' => '<pre>' . check_plain($server->YUM) . '</pre>',
      ];
    }

    if (!empty($server->LASTYUM)) {
      $output['lastyum'] = [
        '#theme' => 'hfcc_global_pseudo_field',
        '#field_name' => 'status_report_lastyum',
        '#label' => t('Last yum'),
        '#label_display' => 'inline',
        '#markup' => format_date(strtotime($server->LASTYUM), 'custom', 'm/d/Y'),
      ];
    }

    return $output;
  }

  /**
   * Get corresponding server node path (if any).
   *
   * @param string $name
   *   The server name to find.
   *
   * @return string|null
   *   The path to the node (if found).
   */
  private function getNodeByName($name) {
    $query = "SELECT nid FROM {node} WHERE UPPER(title) = UPPER(:name) AND type = 'server' AND status = 1";
    $result = db_query($query, [':name' => $name])->fetchCol();
    if (!empty($result)) {
      return url("node/" . reset($result));
    }
  }

  /**
   * Execute a database query.
   *
   * @param string $query
   *   The prepared statement query to run.
   * @param string[] $args
   *   An array of values to substitute into the query.
   *
   * @return StdClass[]
   *   An array of StdClass database objects.
   *
   * @see db_query()
   */
  private function dbQuery($query, $args) {
    $connection_name = variable_get('server_status_database_connection', 'netadmin');
    try {
      return Database::getConnection('default', $connection_name)->query($query, $args)->fetchAll();
    }
    catch (Exception $e) {
      drupal_set_message(t('Error reading server status: %e', array('%e' => $e->getMessage())), 'error');
      return [];
    }
  }

}
