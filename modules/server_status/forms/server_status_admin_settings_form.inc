<?php

/**
 * @file
 * This file contains admin forms for the getpersoninfo module.
 *
 * @see getpersoninfo.module
 */

/**
 * Creates an administrative form for getpersoninfo settings.
 */
function server_status_admin_settings_form($form, &$form_state) {
  $form['server_status_database_connection'] = [
    '#type' => 'textfield',
    '#title' => t('Database Connection Name'),
    '#default_value' => variable_get('server_status_database_connection', 'netadmin'),
    '#required' => TRUE,
  ];

  return system_settings_form($form);
}
